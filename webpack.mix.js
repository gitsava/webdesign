// webpack.mix.js

let mix = require('laravel-mix');

mix.js('src/app.js', 'dist/js')
  .sass('src/app.scss', 'dist/css')
  //.copy('src/fonts', 'dist/fonts')
  .sourceMaps();

if(!mix.inProduction()) {
  mix.sourceMaps();
  mix.webpackConfig({ devtool: 'inline-source-map'})
}
